package com.example.rest;

import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * A resource of message.
 */
@Scanned
@Path("/message")
public class MyRestResource {

    private static ProjectManager projectManager;

    @Inject
    public MyRestResource(@ComponentImport ProjectManager projectManager){
        this.projectManager = projectManager;
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getMessage()
    {
        projectManager.getAllProjectCategories().stream().forEach(System.out::println);
        return Response.ok(new MyRestResourceModel("Hello World")).build();
    }
}